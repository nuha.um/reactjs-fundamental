import React, { Component } from "react";
import './../UpdateParentFromChild/Child.css'
class Child extends Component {
    state = {
        order: 0
    }

    handleCounterChange = (newValue) => {
        this.props.onCounterChange(newValue)
    }
    
    // arrow function this.state reference to global
    handlePlus = () => {
        this.setState({
            order: this.state.order + 1
        }, ()=> {
            this.handleCounterChange(this.state.order)
        })
    }
    handleMinus = () => {
        if (this.state.order > 0) {
            this.setState({
                order: this.state.order - 1
            }, () => {
                this.handleCounterChange(this.state.order)
            })
        }
    }
    // basic function this.state reference to function self
    // handleMinus () {
    //     console.log("Minus");
    // }
    render() {
        return (
            <div className="card">
                Nama Produk [jml: {this.state.order}]
                <br />
                <button onClick={this.handleMinus}>-</button>
                <button onClick={this.handlePlus}>+</button>
            </div>
        )
    }
}
export default Child