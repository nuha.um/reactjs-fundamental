import React, { Component, Fragment } from "react";
import Child from "../ChildToParentUpdate/Child";
import './Child.css'
class Parent extends Component {
    state = {
        order : 0
    }

    handleCounterChange = (newValue) => {
        this.setState({
            order : newValue
        })
    }
    render () {
        return (
            <Fragment>
                <h3>Child</h3>
                <hr />
                <div className="header">
                    tester [keranjang {this.state.order}]
                </div>
                <hr />
                <Child onCounterChange={(value)=> {this.handleCounterChange(value)}} />
            </Fragment>
        )
    }
}
export default Parent