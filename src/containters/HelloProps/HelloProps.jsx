import React, { Component } from "react";
import PropsComponent from "../../components/Props/PropsComponent";

class Home extends Component {
    render () {
        return (
            <div>
                <PropsComponent name="Muhammad Ulin Nuha" umur="18"/>
                <PropsComponent name="Muhammad" umur="19" />
                <PropsComponent name="Ulin Nuha" umur="20" />
                <PropsComponent />
            </div>
        )
    }
}

export default Home