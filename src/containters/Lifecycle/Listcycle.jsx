import React, { Component } from "react";
class Lifecycle extends Component {
    // first
    constructor (props) {
        super(props)
        this.state = {
            count:1,
            source: "https://projects.wojtekmaj.pl/react-lifecycle-methods-diagram/"
        }
        console.log('constructor');
    }
    componentDidMount() {
        console.log('componentDidMount');
        // setTimeout(()=>{
        //     this.setState({
        //         count: 2
        //     })
        // },3000)
    }
    shouldComponentUpdate(nextProps,nextState) {
        // console.log('shouldComponentUpdate');
        // console.group("shouldComponentUpdate");
        // // console.log("nextProps",nextProps);
        // console.log("nextState",nextState);
        // console.log("this.state",this.state);
        // console.groupEnd();
        if (nextState.count > 4) {
            return false
        }
        return true
    }
    getSnapshotBeforeUpdate(prevProps,prevState) {
        console.log('getSnapshotBeforeUpdate');
        return null
    }
    componentDidUpdate(prevProps,prevState,snapshot) {
        console.log('componentDidUpdate');
    }
    componentWillUnmount() {
        console.log('componentWillUnmounts');
    }
    changeCount = () => {
        this.setState({
            count : this.state.count + 1
        })
    }
    render () {
        console.log('render');
        return (
            <div>
                <h3>Lifecycle</h3>
                <hr />
                <button onClick={this.changeCount}>Component Button {this.state.count}</button>
            </div>
        )
    }
}
export default Lifecycle