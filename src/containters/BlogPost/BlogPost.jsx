import React, { Component, Fragment } from 'react'
import Post from '../../components/Post/Post'
import axios from 'axios';
import './BlogPost.css'
import { Link } from "react-router-dom";

class BlogPost extends Component {
    state = {
        post : [],
        formBlogPost : {
            id : 1,
            title : '',
            body : '',
            userId: 1
        },
        isUpdate : false
    }

    getPostApi = () => {
        // axios.get("https://json-server-vercel-nuhaum.vercel.app/posts")
        axios.get("http://localhost:3004/posts?_sort=id&_order=desc")
            .then((result) => {
                this.setState({
                    post: result.data
                })
            })
    }

    postDataToApi = () => {
        axios.post('http://localhost:3004/posts', this.state.formBlogPost)
            .then((res) => {
                this.getPostApi()
                this.setState({
                    formBlogPost: {
                        title: '',
                        body: '',
                        userId: 1
                    },
                    isUpdate: false
                })
            }, (err) => {
                console.log(err)
            })
    }

    putDataTotApi = () => {
        axios.put('http://localhost:3004/posts/' + this.state.formBlogPost.id + '', this.state.formBlogPost)
            .then((res) => {
                this.getPostApi()
                this.setState({
                    formBlogPost: {
                        id: 1,
                        title: '',
                        body: '',
                        userId: 1
                    },
                    isUpdate: false
                })
            })
    }

    handleRemove = (data) => {
        axios.delete('http://localhost:3004/posts/'+data+'')
            .then((res)=>{
                this.getPostApi()
            })
    }
    
    handleUpdate = (data) => {
        console.log(data);
        this.setState({
            formBlogPost: data,
            isUpdate : true
        })
    }

    handleFormChange = (event) => {
        // menyamakan state lama dengan variabel state baru
        let formBlogPostNew = { ...this.state.formBlogPost }
        let timestamp = new Date().getTime()
        if (!this.state.isUpdate) {
            formBlogPostNew['id'] = timestamp
        }
        formBlogPostNew[event.target.name] = event.target.value
        this.setState({
            formBlogPost: formBlogPostNew
        })
    }

    handleSubmit = () => {
        if (this.state.isUpdate) {
            this.putDataTotApi()
        } else {
            this.postDataToApi()
        }
    }

    componentDidMount () {
        this.getPostApi()
    }
    render () {
        return (
            <Fragment>
                <Link to="/lifecycle">
                    <p>Visit your profile</p>
                </Link>
                <p className='section-title'>Blog Post</p>
                <hr />
                <h3>Form Tambah Data</h3>
                <input type="text" name='title' value={this.state.formBlogPost.title} onChange={this.handleFormChange} style={{margin:3,width:200}} placeholder='Add Tittle' />
                <br />
                <textarea name='body' value={this.state.formBlogPost.body} onChange={this.handleFormChange} style={{ margin: 3, width:200 }} placeholder='Add Body' />
                <br />
                <button style={{ margin: 3 }} onClick={this.handleSubmit}> simpan </button>
                {
                    this.state.post.map(post=>{
                        return <Post key={post.id} data={post} remove={this.handleRemove} update={this.handleUpdate} />
                    })
                }
            </Fragment>
        )
    }
}

export default BlogPost