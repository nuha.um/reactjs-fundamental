import React, { Component } from 'react';
import './../../containters/BlogPost/BlogPost.css';
const Post = (props) => {
    return (
        <div className='post'>
            <div className='img-thumb'>
                <img src="https://picsum.photos/200/150" alt="image-dummy" />
            </div>
            <div className='content'>
                <p className='title'>{props.data.title}</p>
                <p className='desc'>{props.data.body}</p>
                <button className='update' onClick={() => props.update(props.data)}>Update</button>
                <button className='remove' onClick={() => props.remove(props.data.id)}>Hapus</button>
            </div>
        </div>
    )
}

export default Post