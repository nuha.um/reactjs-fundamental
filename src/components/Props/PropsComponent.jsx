import React from "react";

const PropsComponent = (props) => {
    return (
        <div>
            <p>Data diambil menggunakan Props</p>
            <ul>
                <li>Nama: {props.name}</li>
                <li>Usia: {props.umur}</li>
            </ul>
        </div>
    )
}

PropsComponent.defaultProps = {
    name: "Default name",
    umur: "Default umur"
}

export default PropsComponent