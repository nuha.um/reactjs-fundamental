import React from 'react'
import { Link } from 'react-router-dom';
const GlobalComponent = () => {
    return (
        <div>
            <nav>
                <ul>
                    <li>
                        <Link to="/">Home</Link>
                    </li>
                    <li>
                        <Link to="/about">About</Link>
                    </li>
                    <li>
                        <Link to="/blogpost">Blogpost</Link>
                    </li>
                    <li>
                        <Link to="/lifecycle">Lifecycle</Link>
                    </li>
                    <li>
                        <Link to="/helloProps">HelloProps</Link>
                    </li>
                </ul>
            </nav>
        </div>
    );
};

export default GlobalComponent;