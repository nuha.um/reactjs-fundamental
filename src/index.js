import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
// import StateFullComponent from './containters/StateFullComponents';
// import StateFullComponent from './containters/StateFullComponent';
// import HelloComponent from './components/HelloComponent';
// import HelloProps from './containters/HelloProps/HelloProps';
// import Child from './containters/UpdateParentFromChild/Parent';
// import Lifecycle from './containters/Lifecycle/Listcycle';
// import BlogPost from './containters/BlogPost/BlogPost';
// import Home from './containters/Home/Home';
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
