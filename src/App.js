// import StateFullComponent from './containters/StateFullComponent';
// import HelloComponent from './components/HelloComponent';
// import HelloProps from './containters/HelloProps/HelloProps';
// import Child from './containters/UpdateParentFromChild/Parent';
import Lifecycle from './containters/Lifecycle/Listcycle';
import BlogPost from './containters/BlogPost/BlogPost';
import Home from './containters/Home/Home';
// import { createBrowserRouter, RouterProvider, Link } from "react-router-dom";
// import GlobalComponent from './components/GlobalComponent';

// const router = createBrowserRouter([
//   {
//     path: "/",
//     element: <Home />,
//   },
//   {
//     path: "/blogpost",
//     element: <BlogPost />,
//   },
//   {
//     path: "about",
//     element: <div>About</div>,
//   },
//   {
//     path: "lifecycle",
//     element: <Lifecycle />,
//   },
//   {
//     path: "helloProps",
//     element: <HelloProps />,
//   },
//   {
//     path: "child",
//     element: <Child />,
//   },
// ]);
import { BrowserRouter, Routes, Route, Link } from 'react-router-dom';

function App() {
  return (
    <div className="App">
      {/* <RouterProvider router={router} element={GlobalComponent} /> */}
      <BrowserRouter>
        <nav>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/about">BlogPost</Link>
            </li>
            <li>
              <Link to="/contact">Contact</Link>
            </li>
          </ul>
        </nav>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/about" element={<BlogPost />} />
          <Route path="/contact" element={<Lifecycle />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
